
import os, sys, shutil
import warnings
from fontbase import GlyphMetrics
import numpy as np

from PIL import Image
from fntdata import FntData
from fontbase import FontBase, RenderMode
from genbase import GenBase

class GenBitmap(GenBase):
    def __init__(self, font: FontBase, config:dict):
        super().__init__(font, config)
        self.font.setRenderMode(RenderMode.Opacity)

        if "font-size" not in config:
            config["font-size"] = 12
            warnings.warn("Font size not set!")
        self.font.setSize(int(config["font-size"]))

        if "font-path" not in config:
            raise RuntimeError("Font path missing.")
        self.font.setPath(config["font-path"])

    def drawGlyphs(self):
        if "image-resolution" not in self.config:
            raise RuntimeError("Missing image-resolution in config.")
        resolution = self.config["image-resolution"]
        if "image-format" not in self.config:
            raise RuntimeError("Missing image-format in config.")
        format = self.config["image-format"]
        # 0,0-------|
        # |         |
        # |---------800,600
        # (0,0, 800, 600)
        # https://pillow.readthedocs.io/en/stable/handbook/concepts.html#concept-modes
        self.images.append(self.createImage(resolution))
        for metric in self.glyphMetrics:
            if metric.empty:
                continue
            
            self.font.loadGlyph(metric.glyph)
            bitmap = self.font.getCurrentBitmap()
            
            dimensions = (metric.width, metric.height)
            image = self.createImage(dimensions)

            if format == "uint8":
                for x in range(image.size[0]):
                    for y in range(image.size[1]):
                        image.putpixel((x,y), int(bitmap[y,x]))
            elif format == "uint16":
                for x in range(image.size[0]):
                    for y in range(image.size[1]):
                        image.putpixel((x,y), int(bitmap[y,x] << 8))
            elif format == "float32":
                for x in range(image.size[0]):
                    for y in range(image.size[1]):
                        normalized = float(bitmap[y,x]) / float(0xFF)
                        image.putpixel((x,y), normalized)

            rect = (metric.x, metric.y, metric.x + metric.imageSourceSize[0], metric.y + metric.imageSourceSize[1])
            self.images[metric.page].paste(image, rect)