
from freetype import *
from ctypes import *
from enum import Enum
import warnings
import numpy as np
from distancemap import DistanceMap

from fontbase import FontBase, FontInfo, RenderMode, GlyphKerning, GlyphMetrics

class FontFreetype(FontBase):
    def __init__(self):
        super().__init__()
    
    def updateInfo(self):
        if self.font != None:
            self.info.name = str(self.font.postscript_name.decode())
            self.info.maxX = int(self.font.size.max_advance / 64)
            self.info.maxY = int(self.font.size.height / 64)

    def setSize(self, size):
        self.info.size = size
        if self.font != None:
            self.font.set_pixel_sizes(self.info.size, self.info.size)
            self.updateInfo()
    
    def setPath(self, path):
        self.info.path = path
        self.font = freetype.Face(path)
        self.font.set_pixel_sizes(self.info.size, self.info.size)
        if not self.font.has_kerning:
            warnings.warn("Kernings not supported!")
        self.updateInfo()
    
    def loadGlyph(self, glyph):
        self.glyph = glyph
        mode = freetype.FT_LOAD_RENDER
        if self.renderMode == RenderMode.Opacity:
            mode = freetype.FT_LOAD_RENDER | freetype.FT_LOAD_TARGET_NORMAL
        elif self.renderMode == RenderMode.Bitmap:
            mode = freetype.FT_LOAD_RENDER | freetype.FT_LOAD_TARGET_MONO
        self.font.load_char(self.glyph, mode)
        return self.font.glyph != None
    
    def getCurrentMetrics(self):
        if self.font == None:
            raise RuntimeError("Font is not loaded.")
        # freetype treats everything as 1/64.
        # so the metrics also use these units.
        metrics = GlyphMetrics()
        metrics.glyph = self.glyph
        #metrics.width = int(self.font.glyph.metrics.width / 64.0)
        #metrics.height = int(self.font.glyph.metrics.height / 64.0)
        if self.font.glyph.metrics.width > 0:
            metrics.empty = False
        
        metrics.width = int(self.font.glyph.bitmap.width)
        metrics.height = int(self.font.glyph.bitmap.rows)

        metrics.imagePackSize = (metrics.width, metrics.height)
        metrics.imageSourceSize = (metrics.width, metrics.height)
        metrics.xAdvance = self.font.glyph.metrics.horiAdvance / 64.0
        metrics.yAdvance = self.font.glyph.metrics.vertAdvance / 64.0

        maxY = int(self.font.size.height / 64)

        metrics.offsetX = self.font.glyph.metrics.horiBearingX / 64.0
        # invert
        metrics.offsetY = maxY - (self.font.glyph.metrics.horiBearingY / 64.0) 

        metrics.kernings = None
        return metrics
    
    def getGlyphKernings(self, metric:GlyphMetrics, glyphs: list):
        if self.font == None:
            raise RuntimeError("Font is not loaded.")
        if not self.font.has_kerning:
            return

        kernings = []
        for glyph in glyphs:
            data = self.font.get_kerning(metric.glyph, glyph)
            if data.x != 0 or data.y != 0:
                kerning = GlyphKerning()
                kerning.next = glyph
                kerning.amount = data.x
                kernings.append(kerning)
        if len(kernings) > 0:
            metric.kernings = kernings

    def getCurrentBitmap(self):
        if self.font == None:
            raise RuntimeError("Font is not loaded.")

        bitmap = self.font.glyph.bitmap
        if bitmap.rows == 0 or bitmap.width == 0:
            return None
        
        # TODO! refactor this horror
        if self.renderMode == RenderMode.Opacity:
            npmap = np.fromiter(bitmap.buffer, dtype=np.uint8)
            return np.reshape(npmap, (-1, bitmap.pitch))[0:bitmap.rows, 0:bitmap.width]
        elif self.renderMode == RenderMode.Bitmap:
            data = DistanceMap((bitmap.width, bitmap.rows))
            for y in range(bitmap.rows):
                for x in range(bitmap.pitch):
                    completed = x * 8
                    remaining = bitmap.width - completed
                    
                    # decode bits from value byte
                    value = bitmap.buffer[y * bitmap.pitch + x]
                    for index in range(0, min(remaining, 8)):
                        bit = value & (1 << (7 - index))
                        data.setValue((completed + index, y), 1 if bit else 0)
            return data
