
import os, sys, shutil
import warnings
from fontbase import GlyphMetrics
import numpy as np
import math

from PIL import Image
from fntdata import FntData
from fontbase import FontBase, RenderMode
from genbase import GenBase
import time

class GenSdf(GenBase):
    def __init__(self, font: FontBase, config:dict):
        super().__init__(font, config)
        self.font.setRenderMode(RenderMode.Bitmap)

        if "font-size" not in config:
            config["font-size"] = 12
            warnings.warn("Font size not set!")

        if "font-size-source" not in config:
            config["font-size-source"] = int(config["font-size"]) * 10
            warnings.warn("Font font-size-source not set!")
        self.font.setSize(int(config["font-size-source"]))

        self.sdfScaling = float(config["font-size-source"]) / float(config["font-size"])
        self.sdfScalingInverse = 1.0 / self.sdfScaling 

        if "font-path" not in config:
            raise RuntimeError("Font path missing.")
        self.font.setPath(config["font-path"])

        if "distance-range" not in config:
            config["distance-range"] = 4
        
    def fixMetrics(self, metrics:GlyphMetrics):
        if "distance-range" not in self.config:
            raise RuntimeError("Missing distance-range in config.")
        distance_range = int(self.config["distance-range"])
        if "image-padding" not in self.config:
            raise RuntimeError("Missing image-padding in config.")
        padding = self.config["image-padding"]

        # scale metrics down
        metrics.width = int(metrics.width * self.sdfScalingInverse)
        metrics.height = int(metrics.height * self.sdfScalingInverse)
        metrics.offsetX *= self.sdfScalingInverse
        metrics.offsetY *= self.sdfScalingInverse
        metrics.xAdvance *= self.sdfScalingInverse
        metrics.yAdvance *= self.sdfScalingInverse

        metrics.imagePackSize = (metrics.width + padding[1] + padding[3] + distance_range + distance_range, metrics.height + padding[0] + padding[2] + distance_range + distance_range)
        metrics.offsetX -= distance_range
        metrics.offsetY -= distance_range
        metrics.width += 2 * distance_range
        metrics.height += 2 * distance_range

    def drawGlyphs(self):
        if "image-resolution" not in self.config:
            raise RuntimeError("Missing image-resolution in config.")
        resolution = self.config["image-resolution"]
        if "distance-range" not in self.config:
            raise RuntimeError("Missing distance-range in config.")
        distance_range = int(self.config["distance-range"])
        if "image-format" not in self.config:
            raise RuntimeError("Missing image-format in config.")
        format = self.config["image-format"]
        
        # 0,0-------|
        # |         |
        # |---------800,600
        # (0,0, 800, 600)
        # https://pillow.readthedocs.io/en/stable/handbook/concepts.html#concept-modes
        self.images.append(self.createImage(resolution))
        count = 0
        for metric in self.glyphMetrics:
            if metric.empty:
                continue

            tic = time.perf_counter()
            
            self.font.loadGlyph(metric.glyph)
            distanceMap = self.font.getCurrentBitmap()
            
            dimensions = (metric.width, metric.height)
            image = self.createImage(dimensions)
            
            for x in range(image.size[0]): # for every pixel:
                for y in range(image.size[1]):
                    # map full rectangle, to the bitmap coordinate space
                    mapped = ((x - distance_range) * self.sdfScaling, (y - distance_range) * self.sdfScaling)

                    distance = distanceMap.getDistance(mapped) * self.sdfScalingInverse
                    
                    # normalize against max range.
                    normalizedRange = distance / float(distance_range)
                    # cut the excess
                    normalized = max(min(normalizedRange, 1.0), -1.0)

                    # invert the positive range.
                    inverted = 1.0 - normalized
                    packed = inverted / 2.0 

                    if format == "uint8":
                        image.putpixel((x,y), (int(packed * 0xFF)))
                    elif format == "uint16":
                        # apparently pillow is made by drunken mongoose, 16bit max is 0xFFFF???
                        image.putpixel((x,y), (int(packed * 0xFFFF)))
                    elif format == "float32":
                        image.putpixel((x,y), distance)

            toc = time.perf_counter()
            count += 1
            print(f"Processed {count}/{len(self.glyphMetrics)}. Took {toc - tic:0.4f} seconds")
            rect = (metric.x, metric.y, metric.x + dimensions[0], metric.y + dimensions[1])
            self.images[metric.page].paste(image, rect)