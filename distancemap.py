
import numpy as np
import math

def calculateDistance(p, p2):
    return math.sqrt(math.pow(abs(float(p[0]) - float(p2[0])), 2.0) + math.pow(abs(float(p[1]) - float(p2[1])), 2.0))

class DistanceMap:
    def __init__(self, dimension):
        self.dim = dimension
        self.data = np.arange(self.dim[1] * self.dim[0], dtype=np.uint8).reshape(self.dim[1], self.dim[0])

    def generate(self):
        # generate summed area table with the data
        self.data = self.data.cumsum(axis=0).cumsum(axis=1)
        pass

    def setValue(self, position, value):
        self.data[position[1], position[0]] = value

    def getValue(self, position):
        if position[0] < 0 or position[0] >= self.dim[0] or position[1] < 0 or position[1] >= self.dim[1]:
            return 0
        ipos = (int(position[0] + 0.5), int(position[1] + 0.5))
        return self.data[ipos[1], ipos[0]]
    
    def getDistanceToEdge(self, position):
        edge = position[0]
        if edge > position[1]:
            edge = position[1]
        farX = abs(float(self.dim[0]) - float(position[0]))
        if edge > farX:
            edge = farX
        farY = abs(float(self.dim[1]) - float(position[1]))
        if edge > farY:
            edge = farY
        return edge

    # returns distance to the edge, inside the shape values are negative.
    def getDistance(self, position):
        distance = float("inf")

        point = self.getValue(position)
        if point == 1:
            distance = self.getDistanceToEdge(position)

        # lets try manhattan distance circle
        max = self.dim[0] if self.dim[0] > self.dim[1] else self.dim[1]
        for dIndex in range(1, max):
            d1 = (position[0] - dIndex, position[1] + dIndex)
            d2 = (position[0] + dIndex, position[1] + dIndex)
            d3 = (position[0] + dIndex, position[1] - dIndex)
            d4 = (position[0] - dIndex, position[1] - dIndex)

            side = int(2 * dIndex)
            for index in range(side):
                p1 = (d1[0] + index, d1[1])
                p2 = (d2[0], d2[1] - index)
                p3 = (d3[0] - index , d3[1])
                p4 = (d4[0], d4[1] + index)

                if self.getValue(p1) != point:
                    dist = calculateDistance(position, p1)
                    if dist < distance:
                        distance = dist
                if self.getValue(p2) != point:
                    dist = calculateDistance(position, p2)
                    if dist < distance:
                        distance = dist
                if self.getValue(p3) != point:
                    dist = calculateDistance(position, p3)
                    if dist < distance:
                        distance = dist
                if self.getValue(p4) != point:
                    dist = calculateDistance(position, p4)
                    if dist < distance:
                        distance = dist
            if distance < dIndex:
                break
        
        return distance if point == 0 else -distance