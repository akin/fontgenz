
import os

import fontgenz
from fntdata import FntData
from fontbase import FontBase
from fontfreetype import FontFreetype
from genbitmap import GenBitmap
from gensdf import GenSdf
import time

configuration = {
    "font-path": "resources/NotoSans-Regular.ttf",
    "font-size": 35,
    "font-size-source": 350,
    "image-resolution": (1024, 1024),
    "image-format": "uint16",
#    "image-post-process": "gaussian-blur(1)",
    "image-padding": (1, 1, 1, 1), # up, right, down, left
    "distance-range": 5,
    "mode": "sdf"
}

tic = time.perf_counter()
font = fontgenz.getFont(configuration)
generator = fontgenz.getGenerator(font, configuration)

debug = False
if debug:
    # A B C
    generator.loadGlyphRange(65, 67)
else:
    # Latin
    generator.loadGlyphRange(32, 126)
    # Latin-1 supplement
    generator.loadGlyphRange(160, 255)
    # Latin extended a
    generator.loadGlyphRange(256, 383)
    # Latin extended b
    generator.loadGlyphRange(384, 591)
    # Spacing modifier letters
    generator.loadGlyphRange(688, 767)
    # Combination diacritical marks
    generator.loadGlyphRange(768, 879)
    # Specials
    generator.loadGlyphRange(65520, 65535)

exporter = fontgenz.getExporter(configuration)
generator.process(exporter)

exporter.write("resources/noto")
toc = time.perf_counter()

print(f"Finished in {toc - tic:0.4f} seconds")