
import os
from PIL import Image
from freetype import GlyphMetrics
from fontbase import FontInfo
import json

class FntData:
    def __init__(self, config:dict):
        self.config = config
        self.images = []
        self.glyphMetrics = []
        self.info = None
    
    def addImage(self, image: Image):
        self.images.append(image)
        
    def setGlyph(self, metric: GlyphMetrics):
        self.glyphMetrics.append(metric)

    def setInfo(self, info: FontInfo):
        self.info = info

    def write(self, path):
        if "image-resolution" not in self.config:
            raise RuntimeError("Missing image-resolution in config.")
        resolution = self.config["image-resolution"]
        if "image-padding" not in self.config:
            raise RuntimeError("Missing image-padding in config.")
        padding = self.config["image-padding"]
        if "image-format" not in self.config:
            raise RuntimeError("Missing image-format in config.")
        format = self.config["image-format"]

        if "mode" not in self.config:
            raise RuntimeError("Missing mode in config.")

        self.config["output-file"] = os.path.basename(path)

        outdata = {}
        
        outdata["info"] = {
            "face": self.info.name,
            "size": int(self.info.size),
            "charset": [],
            "unicode": 1,
            "bold": 1 if self.info.bold else 0,
            "italic": 1 if self.info.italic else 0,
            "smooth": 1 if self.info.smooth else 0,
            "aa": 1 if self.info.antialias else 0,
            "stretchH": int(self.info.stretchHorizontal),
            "padding": padding,
            "spacing": self.info.spacing
        }

        mode = self.config["mode"]
        if mode == "sdf":
            if "distance-range" not in self.config:
                raise RuntimeError("Missing distance-range in config.")
            outdata["distanceField"] = {
                "fieldType": mode,
                "distanceRange": int(self.config["distance-range"])
            }

        outdata["pages"] = []
        outdata["chars"] = []
        outdata["kernings"] = []
        outdata["common"] = {
            "lineHeight": self.info.maxY,
            "base": self.info.base,
            "scaleW": resolution[0],
            "scaleH": resolution[1],
            "pages": len(self.images),
            "packed": 1 if self.info.packed else 0,
            "alphaChnl": 1 if self.info.alpha else 0,
            "redChnl": 1 if self.info.red else 0,
            "greenChnl": 1 if self.info.green else 0,
            "blueChnl": 1 if self.info.blue else 0
        }

        # Pages
        for index in range(0, len(self.images)):
            # pillow cant handle 32bit float images with png.
            imageFormat = ".png"
            if format == "float32":
                imageFormat = ".tiff"
            
            imagepath = path + '_' + str(index) + imageFormat
            fullPath = os.path.join(os.getcwd(), imagepath)
            self.images[index].save(fullPath)
            outdata["pages"].append(os.path.basename(imagepath))

        # Chars
        for metric in self.glyphMetrics:
            char = {
                "id": int(metric.glyph),
                "index": int(metric.index),
                "char": chr(metric.glyph),
                "width": int(metric.width),
                "height": int(metric.height),
                "xoffset": int(metric.offsetX),
                "yoffset": int(metric.offsetY),
                "xadvance": int(metric.xAdvance),
                "chnl": int(metric.channel),
                "x": int(metric.x),
                "y": int(metric.y),
                "page": int(metric.page),
            }
            outdata["chars"].append(char)

            if metric.kernings != None:
                for kerning in metric.kernings:
                    outdata["kernings"].apped({
                        "first": int(metric.glyph),
                        "second": int(kerning.next),
                        "amount": int(kerning.amount)
                    })
            outdata["info"]["charset"].append(chr(metric.glyph))
        
        fullPath = os.path.join(os.getcwd(), path + ".json")
        with open(fullPath, 'w') as outfile:
            json.dump(outdata, outfile, indent=4)
        fullPath = os.path.join(os.getcwd(), path + ".config.json")
        with open(fullPath, 'w') as outfile:
            json.dump(self.config, outfile, indent=4)