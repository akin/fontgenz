
import os, sys, shutil
from fontbase import GlyphMetrics
import numpy as np

import warnings
from PIL import Image, ImageFilter
from fntdata import FntData
from fontbase import FontBase, RenderMode
import rpack
import re

class GenBase:
    def __init__(self, font: FontBase, config:dict):
        self.config = config
        self.font = font
        self.font.setRenderMode(RenderMode.Opacity)
        self.glyphMetrics = []
        self.glyphs = []
        self.images = []

    def loadGlyph(self, glyph: int):
        # avoid duplicates
        res = [item for item in self.glyphs if(item == glyph)]
        if not res:
            self.glyphs.append(glyph)

    def loadGlyphs(self, object):
        if isinstance(object, str):
            for c in object:
                self.loadGlyph(ord(c))
            return
        elif isinstance(object, list):
            for item in object:
                self.loadGlyph(item)
            return
        raise RuntimeError("Unknown object " + str(object))

    def loadGlyphRange(self, start: int, end: int):
        for glyph in range(start, end + 1):
            self.loadGlyph(glyph)

    def createImage(self, dimensions):
        if "image-format" not in self.config:
            raise RuntimeError("Missing image-format in config.")
        format = self.config["image-format"]

        if format == "uint8":
            selected = 'L'
        elif format == "uint16":
            selected = 'I;16'
        elif format == "float32":
            selected = 'F'
        return Image.new(selected, dimensions)
    
    def fixMetrics(self, metrics:GlyphMetrics):
        if "image-padding" not in self.config:
            raise RuntimeError("Missing image-padding in config.")
        padding = self.config["image-padding"]
        metrics.imagePackSize = (metrics.width + padding[1] + padding[3], metrics.height + padding[0] + padding[2])
    
    def setGlyphMetricPosition(self, metrics:GlyphMetrics, x: int, y: int, page: int):
        if "image-padding" not in self.config:
            raise RuntimeError("Missing image-padding in config.")
        padding = self.config["image-padding"]
        metrics.x = int(x + padding[3])
        metrics.y = int(y + padding[0])
        metrics.page = int(page)
    
    def packGlyphs(self, fntdata: FntData):
        if "image-resolution" not in self.config:
            raise RuntimeError("Missing image-resolution in config.")
        resolution = self.config["image-resolution"]

        # https://pypi.org/project/rectangle-packer/
        rectangles = []
        glyphs = []
        for metric in self.glyphMetrics:
            if metric.empty:
                continue
            glyphs.append(metric)
            rectangles.append(metric.imagePackSize)
        
        positions = rpack.pack(rectangles, max_width=resolution[0], max_height=resolution[1])
        for index in range(0, len(glyphs)):
            pos = positions[index]
            self.setGlyphMetricPosition(glyphs[index], pos[0], pos[1], 0)
    
    def drawGlyphs(self):
        raise RuntimeError("drawGlyphs should be overridden")
    
    def postProcess(self, image:Image):
        if "image-format" not in self.config:
            raise RuntimeError("Missing image-format in config.")
        format = self.config["image-format"]

        if "image-post-process" not in self.config:
            return image
        commands = self.config["image-post-process"].split(',')
        for command in commands:
            if command.startswith( 'gaussian-blur' ):
                if format != "uint8":
                    warnings.warn("Blur not supported on anything else than 8bit images!")
                    continue
                found = re.search(r'(\d+)', command)
                if found:
                    image = image.filter(ImageFilter.GaussianBlur(int(found.groups()[0])))

        return image
    
    def process(self, fntdata: FntData):
        # load metrics.
        for index in range(0, len(self.glyphs)):
            glyph = self.glyphs[index]
            self.font.loadGlyph(glyph)
            metrics = self.font.getCurrentMetrics()
            metrics.index = index
            self.glyphMetrics.append(metrics)

        self.config["glyphs"] = self.glyphs
        
        # get kernings
        for metrics in self.glyphMetrics:
            self.font.getGlyphKernings(metrics, self.glyphs)

        for metrics in self.glyphMetrics:
            self.fixMetrics(metrics)

        fntdata.setInfo(self.font.getFontInfo())
        self.packGlyphs(fntdata)
        self.drawGlyphs()
        for metrics in self.glyphMetrics:
            fntdata.setGlyph(metrics)
        for image in self.images:
            fntdata.addImage(self.postProcess(image))
