from fntdata import FntData
from fontfreetype import FontFreetype
from genbitmap import GenBitmap
from gensdf import GenSdf
import time
import json

def getFont(configuration):
    return FontFreetype()
    
def getExporter(configuration):
    return FntData(configuration)

def getGenerator(font, configuration):
    if configuration == None:
        raise RuntimeError("Missing configuration!")
    if font == None:
        raise RuntimeError("Missing font!")

    # default mode
    if "mode" not in configuration:
        configuration["mode"] = "bitmap"

    # supported modes:
    if configuration["mode"] == "bitmap":
        return GenBitmap(font, configuration)
    elif configuration["mode"] == "sdf":
        return GenSdf(font, configuration)
    raise RuntimeError("unknown mode " + configuration["mode"])

def run(configuration):
    tic = time.perf_counter()
    font = getFont(configuration)
    generator = getGenerator(font, configuration)

    if "glyphs" in configuration:
        generator.loadGlyphs(configuration["glyphs"])

    exporter = getExporter(configuration)
    generator.process(exporter)

    if "output-file" in configuration:
        exporter.write(configuration["output-file"])

    toc = time.perf_counter()
    print(f"Finished in {toc - tic:0.4f} seconds")

if __name__ == "__main__":
    import argparse as arggs
    
    arguments_parser = arggs.ArgumentParser(description='Generate fontz')
    arguments_parser.add_argument('-i','--in', help='input configuration file', required=False)
    arguments_parser.add_argument('-o','--out', help='output files name', required=False)
    arguments_parser.add_argument('-g','--glyphs', help='output glyphs', required=False)
    arguments_parser.add_argument('-f','--font-path', help='font path', required=False)
    arguments_parser.add_argument('-s','--font-size', help='font size', required=False)
    arguments_parser.add_argument('-z','--font-size-source', help='font source size', required=False)
    arguments_parser.add_argument('-w','--image-width', help='output image width', required=False)
    arguments_parser.add_argument('-h','--image-height', help='output image height', required=False)
    arguments_parser.add_argument('--image-format', help='output image format (uint8, uint16, float32)', required=False)
    arguments_parser.add_argument('--image-post-process', help='post processing applied to the output image (gaussian-blur(n))', required=False)
    arguments_parser.add_argument('-p', '--padding', help='padding around each glyph', required=False)
    arguments_parser.add_argument('--padding-top', help='padding top for each glyph', required=False)
    arguments_parser.add_argument('--padding-bottom', help='padding bottom for each glyph', required=False)
    arguments_parser.add_argument('--padding-left', help='padding left for each glyph', required=False)
    arguments_parser.add_argument('--padding-right', help='padding right for each glyph', required=False)
    arguments_parser.add_argument('--distance-range', help='distance glyph max range', required=False)
    arguments_parser.add_argument('-m', '--mode', help='glyph generation mode (bitmap, sdf)', required=False)
    args = vars(arguments_parser.parse_args())

    configuration = {}

    # first parse input file
    if 'in' in args:
        with open(args['in']) as json_file:
            configuration = json.load(json_file)

    # Command line arguments overrides configuration input
    if 'out' in args:
        configuration["output-file"] = args['out']
    if 'glyphs' in args:
        configuration["glyphs"] = args['glyphs']
    if 'font-path' in args:
        configuration["font-path"] = args['font-path']
    if 'font-size' in args:
        configuration["font-size"] = int(args['font-size'])
    if 'font-size-source' in args:
        configuration["font-size-source"] = int(args['font-size-source'])

    ## image resolution
    imageRes = (0,0)
    if "image-resolution" in configuration:
        imageRes = configuration["image-resolution"]
    if 'image-width' in args:
        imageRes[0] = int(args['image-width'])
    if 'image-height' in args:
        imageRes[1] = int(args['image-height'])
    configuration["image-resolution"] = imageRes

    if 'image-format' in args:
        configuration["image-format"] = args['image-format']
    if 'image-post-process' in args:
        configuration["image-post-process"] = args['image-post-process']

    ## padding
    # up, right, down, left
    padding = (1,1,1,1)
    if "image-padding" in configuration:
        padding = configuration["image-padding"]
    if 'padding' in args:
        pad = int(args['padding'])
        padding = (pad, pad, pad, pad)
    if 'padding-top' in args:
        padding[0] = int(args['padding-top'])
    if 'padding-bottom' in args:
        padding[2] = int(args['padding-bottom'])
    if 'padding-left' in args:
        padding[3] = int(args['padding-left'])
    if 'padding-right' in args:
        padding[1] = int(args['padding-right'])
    configuration["image-padding"] = padding

    if 'distance-range' in args:
        configuration["distance-range"] = int(args['distance-range'])
    if 'mode' in args:
        configuration["mode"] = args['mode']

    run(configuration)