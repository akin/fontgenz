
def convert(input: str, output: str):
    # input types: .fnt, .json, .xml
    # output types: .fnt, .json, .xml
    pass

if __name__ == "__main__":
    import argparse as arggs
    
    arguments_parser = arggs.ArgumentParser(description='Convert fnt files')
    arguments_parser.add_argument('-i','--in', help='input file', required=True)
    arguments_parser.add_argument('-o','--out', help='output file', required=True)
    args = vars(arguments_parser.parse_args())
    convert(args['in'], args['out'])