
from freetype import *
from ctypes import *
from enum import Enum

import numpy as np

class FontInfo:
    def __init__(self):
        self.name = ""
        self.size = 0
        self.path = ""
        self.bold = False
        self.italic = False
        self.stretchHorizontal = 0
        self.smooth = True
        self.antialias = True
        self.spacing = (1,1)
        self.base = 0
        self.packed = False
        self.red = False
        self.green = False
        self.blue = False
        self.alpha = False
        self.maxX = 0
        self.maxY = 0

class GlyphKerning:
    def __init__(self):
        self.next = -1
        self.amount = 0

class GlyphMetrics:
    def __init__(self):
        self.index = -1
        self.empty = True
        self.x = -1
        self.y = -1
        self.page = -1
        self.channel = -1
        self.glyph = 0
        self.width = 0
        self.height = 0
        self.imagePackSize = (0,0)
        self.imageSourceSize = (0,0)
        self.xAdvance = 0
        self.yAdvance = 0
        self.offsetX = 0
        self.offsetY = 0
        self.kernings = []

class RenderMode(Enum):
     Bitmap = 1
     Opacity = 2

class FontBase:
    def __init__(self):
        self.info = FontInfo()
        self.renderMode = RenderMode.Opacity
        self.glyph = None
        self.font = None

    def updateInfo(self):
        raise RuntimeError("should be overridden")
    
    def setSize(self, size):
        raise RuntimeError("should be overridden")
    
    def setPath(self, path):
        raise RuntimeError("should be overridden")
    
    def setRenderMode(self, mode: RenderMode):
        self.renderMode = mode
    
    def loadGlyph(self, glyph):
        raise RuntimeError("should be overridden")
    
    def getCurrentMetrics(self):
        raise RuntimeError("should be overridden")
    
    def getGlyphKernings(self, metric:GlyphMetrics, glyphs: list):
        raise RuntimeError("should be overridden")
    
    def getFontInfo(self):
        if self.font == None:
            raise RuntimeError("Font is not loaded.")
        return self.info

    def getCurrentBitmap(self):
        raise RuntimeError("should be overridden")