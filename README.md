# FontGenZ
Tool for converting TTF fonts to FNT fonts

 * tries to be bmfont compatible
 * json variant of bmfont data (as it is easy)
 * simple python

pip install -r requirements.txt
---
inspiration:

 * https://www.angelcode.com/products/bmfont/
 * https://github.com/vladimirgamalyan/fontbm
 * https://github.com/Jam3/msdf-bmfont
 * https://github.com/Chlumsky/msdfgen
 * https://github.com/Chlumsky/msdf-atlas-gen

## Tasks 
### DONE 
 * load ttf font 
 * load glyphs out of the font
 * save loaded glyphs as bitmap (png) + fnt format file
 * license
 * move to own repo
 * add kerning parsing
 * save loaded glyphs as signed distance field (png) + fnt format file
 * add higher resolution luminance bitmaps
 * better SDF calculations, current one is UGLY

### TODO
 * refactor SDF rendering to do sdf so that is actually uses SDF as signed distance fields.
 * save loaded glyphs as multilayer signed distance field (png) + fnt format file
 * add command line parameters support
 * add multi texture support
 * add compressinator support
 * librarify the project
 * optimize, python is the slowest language on earth, so might make sense to pick low hanging fruits. (single SDF glyph at 35pixels takes 151 seconds to calculate)